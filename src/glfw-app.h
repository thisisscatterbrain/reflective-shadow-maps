/*
 * glfw-app.h
 *
 * GLFW convenience wrapper
 */
#ifndef GLFW_APP_H
#define GLFW_APP_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>


void glfw_app_initialize();
void glfw_app_run();
void glfw_app_finalize();

#endif /* end of include guard : glfw-app.h */