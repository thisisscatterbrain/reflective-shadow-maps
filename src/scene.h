/* scene.h */
#ifndef SCENE_H
#define SCENE_H

#include <GL/glew.h>

struct material {
	float diffuse_reflectance[3];
};

struct mesh {
	GLuint vao;
	GLuint vbo;
	struct material material;
	struct mesh * next;
};

struct scene {
	struct mesh * root;
};

/* Loads scene from an .obj file. The scenes faces are stored in seperate meshes
 * according to the material of the face. */
void scene_initialize();
void scene_finalize();

#endif /* end of include guard : scene.h */