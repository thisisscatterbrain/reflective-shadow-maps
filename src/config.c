#define CONFIG
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char * find_value(int argc, const char * argv[], char * attribute)
{
	char str[256];
	char * r = NULL;
	int i = 0;
	sprintf(str, "--%s=", attribute);

	for (; i < argc; i++) {
		r = strstr(argv[i], str);

		if (r) {
			return r + strlen(str);
		}
	}

	return r;
}

static void set_defaults()
{
	strcpy(config.scene.path, ".");
}

static void print_usage(const char * appname) 
{
	printf("usage : %s <.obj-filename> [--path=<path>]\n", appname);
}

void config_initialize(int argc, char * argv[])
{
	char * val = NULL;
	
	if (argc == 1) {
		print_usage(argv[0]);
		exit(EXIT_FAILURE);
	}

	strncpy(config.scene.filename, argv[1], 256);
	set_defaults();


	val = find_value(argc, argv, "path");
	
	if (val)
		strncpy(config.scene.path, val, 256);
}


void config_finalize() {}