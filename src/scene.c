#include "scene.h"
#include "config.h"
#include "ext/cobj/obj.h"
#include <stdio.h>
#include <stdlib.h>

/* Temporary storage for the meshes on host. */
struct local_mesh {
	float * positions;
	float * normals;
	int face_count;
	struct material materials;
};

static void count_faces_per_material(obj_scene_t scene, 
	struct local_mesh * material_local_mesh_map)
{
	int i = 0;
	int nfaces = obj_scene_get_face_count(scene);
	int matid;	

	for (; i < nfaces; i++) {
		obj_scene_get_material(scene, &matid, i);
		material_local_mesh_map[i].face_count++;
	}
}

static void allocate(struct local_mesh * material_local_mesh_map,
	int mesh_count)
{
	int i = 0;
	size_t size;

	for (; i < mesh_count; i++) {
		size = material_local_mesh_map[i].face_count * 3 * 3 * sizeof(float);
		material_local_mesh_map[i].positions = malloc(size);
		material_local_mesh_map[i].normals = malloc(size);
	}
}

static void release(struct local_mesh * material_local_mesh_map,
	int mesh_count)
{
	int i = 0;

	for (; i < mesh_count; i++) {
		free(material_local_mesh_map[i].positions);
		free(material_local_mesh_map[i].normals);
	}
}

static void set_materials(obj_scene_t scene, 
	struct local_mesh * material_local_mesh_map,
	int mesh_count)
{
	int i = 0;

	for (; i < mesh_count; i++) {
		obj_scene_material_get_element_for_entry_as_float(
			scene, i, &material_local_mesh_map[i].materials.diffuse_reflectance[0], "Kd", 0);
		obj_scene_material_get_element_for_entry_as_float(
			scene, i, &material_local_mesh_map[i].materials.diffuse_reflectance[1], "Kd", 1);
		obj_scene_material_get_element_for_entry_as_float(
			scene, i, &material_local_mesh_map[i].materials.diffuse_reflectance[1], "Kd", 2);
	}
}

static void copy_geometry(obj_scene_t scene, 
	struct local_mesh * material_local_mesh_map,
	int mesh_count)
{
	/* TODO: */
}

void scene_initialize()
{
	struct local_mesh * matmeshmap;
	int nmeshes;
	obj_scene_t scene = obj_scene_create_with_file(
		config.scene.filename, config.scene.path);

	nmeshes = obj_scene_get_material_count(scene);
	matmeshmap = calloc(nmeshes, sizeof(*matmeshmap));

	count_faces_per_material(scene, matmeshmap);
	allocate(matmeshmap, nmeshes);
	set_materials(scene, matmeshmap, nmeshes);


	release(matmeshmap);
	free(matmeshmap);
	obj_scene_release(scene);
}

void scene_finalize()
{

}