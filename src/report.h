/* 
 * report.h
 *
 * Declares functions to report warnings and errors. 
 */
#ifndef REPORT_H
#define REPORT_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdio.h>
#include <stdarg.h>

/* Prints a warning. */
void print_info(const char * const format, ...);

/* Prints a warning. */
void print_warning(const char * const format, ...);

/* Sets function to call if the program dies. */
void set_finalize_function(void (*finalize)(void));

/* Print a error message, finalize and exit. */
void die(const char * const format, ...);

#ifdef __cplusplus
}
#endif

#endif /* end of include guard: REPORT_H */
