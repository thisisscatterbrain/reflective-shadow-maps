#include "glfw-app.h"
#include "report.h"
#include <assert.h>

#define MAX_STRLEN 256

static GLFWwindow * window;
static int width = 1280;
static int height = 720;
static int is_initialized = 0;
static char title[MAX_STRLEN] = {"GLFW Application"};

void glfw_app_initialize()
{
	if (is_initialized)
		die("Application is already initialized");

	/* init glfw */
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	window = glfwCreateWindow(width, height, "hi test", NULL, NULL);
	glfwMakeContextCurrent(window);

	/* init glew */
	glewExperimental = GL_TRUE;
	glewInit();
	glGetError();
}

void glfw_app_finalize()
{
	if (!is_initialized)
		die("Application not initialized");

	glfwTerminate();
}

void glfw_app_run()
{
	while (!glfwWindowShouldClose(window)) {
		glfwSwapBuffers(window);
		glfwPollEvents();
	}
}