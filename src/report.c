#include "report.h"
#include <stdlib.h>

/*****************************************************************************/
static void (*_finalizefp)(void) =  NULL;

/*****************************************************************************/

void print_info(const char * const format, ...)
{
	va_list args;
	fprintf(stderr, "I\t ");
	va_start(args, format);
	vfprintf(stderr, format, args);
	va_end(args);	
	fprintf(stderr, "\n");
}

void print_warning(const char * const format, ...)
{
	va_list args;
	fprintf(stderr, "W\t ");
	va_start(args, format);
	vfprintf(stderr, format, args);
	va_end(args);	
	fprintf(stderr, "\n");
}

void set_finalize_function(void (*finalizefp)(void))
{
	_finalizefp = finalizefp;
}

void die(const char * const format, ...)
{
	va_list args;
	
	fprintf(stderr, "E\t ");
	va_start(args, format);
	vfprintf(stderr, format, args);
	va_end(args);	
	fprintf(stderr, "\n");

	if (_finalizefp)
		(*_finalizefp)();

	exit(EXIT_FAILURE); 
}
