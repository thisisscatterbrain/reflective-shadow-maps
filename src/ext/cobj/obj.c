/*
** obj.c
*/
#include "obj.h"
#include <stdio.h>
#include <assert.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

#define MAX_STRLEN 256

/*****************************************************************************/

/* A dynamic array structure */
struct array {
	char * data;
	int elem_size;		/* size of one element */
	size_t count;
	size_t capacity;
};

struct obj_material_entry {
	char name[MAX_STRLEN];
	char ** elements;
	size_t element_count;
	struct obj_material_entry * next;
};

struct obj_material {
	char name[MAX_STRLEN];
	struct obj_material_entry * entries[32];
};

/* Represents a .obj scene. */
struct obj_scene {
	/* a face is an int[4] array. the first three entries refer to the 
	 * face's vertices, the last one to the faces material. */
	struct array * faces;

	/* a vertex is a int[3] array. each entry (if not - 1) refers to position
	 * texture coordinate and normal. */
	struct array * vertices;
	struct array * positions;
	struct array * normals;
	struct array * tex_coords;

	/* stores pointer to materials */
	struct array * materials;
};

/*****************************************************************************/

static char current_material[MAX_STRLEN];
static char mtl_file_name[MAX_STRLEN];

/*****************************************************************************/
/*
 * Computes the hash for a string according to the djb2 algorithm. (see:
 * http://www.cse.yorku.ca/~oz/hash.html)
 */ 
size_t get_hash(const char * str)
{
	unsigned long hash = 5381;
	char c;

	while (1) {
		c = *str++;

	    	if (c == '\0') {
			break;
		}

		hash = ((hash << 5) + hash) + c; /* hash * 33 + cse */
	}

        return hash;
}

static void trim_line(char * trimmed_line, const char * line)
{
	int i = 0, j = 0;
	int ignsp = 0;

	while (isspace(line[i])) {
		i++;	
	}

	while (line[i] != EOF && line[i] != '\n' && line[i] != '#') {
		if (!(ignsp && isspace(line[i]))) {
			trimmed_line[j] = line[i];
			j++;
		}

		if (isspace(line[i])) {
			ignsp = 1;
		} else {
			ignsp = 0;
		}

		i++;	
	}

	trimmed_line[j] = '\0';
}

/*****************************************************************************/

static struct array * array_create(int elem_size, size_t count)
{
	struct array * a = calloc(1, sizeof(*a));
	assert(a && (elem_size > 0) && (count >= 0));
	a->elem_size = elem_size;
	a->count = count;

	/* allocate at least space for one element ... */
	if (count == 0)
		count++;

	a->data = calloc(elem_size, count);
	a->capacity = elem_size * count;
	assert(a);
	return a;
}

static void * array_get(struct array * a, int i)
{
	assert(a && (i >= 0));
	return a->data + i * a->elem_size;
}

static void array_push(struct array * a, void * data)
{
	if ((a->count + 1) * a->elem_size > a->capacity) {
		a->capacity *= 2;
		a->data = realloc(a->data, a->capacity * 2);
		assert(a->data);
	}

	memcpy(a->data + a->count * a->elem_size, data, a->elem_size);
	a->count++;
}

static void array_release(struct array * a)
{
	free(a->data);
	free(a);
}

/*****************************************************************************/

/* counts the number of elements a entry holds, given a corresponding line from
 * an .mtl file. */
static size_t get_element_count(const char * line)
{
	size_t i = 0;
	size_t cnt = 0;
	
	while (1) {
		if (line[i] == ' ')
			cnt++;
		
		if (line[i] == '\0')
			break;
	
		i++;
	}
	
	return cnt;
}

static char ** create_elements(const char * line)
{
	char l[MAX_STRLEN];
	size_t nelems = get_element_count(line);
	char ** elems = calloc(nelems, sizeof(char*));
	char * tok;
	size_t i = 0;
	strcpy(l, line);
	assert(nelems);
	
	tok = strtok(l, " ");

	while (tok) {
		tok = strtok(NULL, " ");

		if (!tok)
			break;

		elems[i] = malloc(strlen(tok) + 1);
		strcpy(elems[i], tok);
		i++;
	}
	
	return elems;
}

/* gets the name of a material entry from a .mtl file's line */
static void get_entry_name(char * name, const char * line)
{
	int i = 0;
	
	while (!isspace(line[i]) && line[i] != EOF && line[i] != '\n')
		i++;
	
	memcpy(name, line, i);
	name[i] = '\0';
}

/* creates and adds an entry to a material. */
static struct obj_material_entry * create_material_entry(
	struct obj_scene * scene, int material, const char * line)
{
	struct obj_material ** m = array_get(scene->materials, material);
	struct obj_material_entry * me = NULL;
	char name[MAX_STRLEN];
	int hash;
	
	me = calloc(1, sizeof(*me));
	get_entry_name(name, line);
	strcpy(me->name, name);
	me->element_count = get_element_count(line);
	me->elements = create_elements(line);
	hash = get_hash(name) % 32;
	me->next = (*m)->entries[hash];
	(*m)->entries[hash] = me;
	return me;
}

static void release_material_entry(struct obj_material_entry * entry)
{
	size_t i = 0;
	
	for (; i < entry->element_count; i++) {
		free(entry->elements[i]);
	}
	
	free(entry->elements);
	free(entry);
}

static struct obj_material_entry * find_material_entry(
	struct obj_material * material, const char * entry_name)
{
	size_t hash = get_hash(entry_name) % 32;
	struct obj_material_entry * e = material->entries[hash];
	
	while (e) {
		if (!strcmp(e->name, entry_name))
			return e;
		e = e->next;
	}
	
	return NULL;
}

/*****************************************************************************/

static struct obj_material * create_material(const char * name)
{
	struct obj_material * m = calloc(1, sizeof(*m));
	strcpy(m->name, name);
	return m;
}

static void release_material(struct obj_material * material)
{
	int i = 0;
	struct obj_material_entry * me = NULL;
	struct obj_material_entry * men = NULL;

	for (; i < 32; i++) {
		me = material->entries[i];

		while (me) {
			men = me->next;
			release_material_entry(me);
			me = men;
		}
	}

	free(material);
}

static int find_material_index(const struct obj_scene * scene, 
	const char * name)
{
	struct obj_material ** m = NULL;
	int i = 0;

	for (; i < (int)scene->materials->count; i++) {
		m = array_get(scene->materials, i);

		if (!strcmp((*m)->name, name)) {
			return i;
		}
	}

	return -1;
}

static struct obj_material * find_material(struct obj_scene * scene, 
	const char * name)
{
	int idx = find_material_index(scene, name);
	struct obj_material ** m = NULL;
	
	if (idx < 0)
		return NULL;
	
	m = array_get(scene->materials, idx);
	return *m;
}

/*****************************************************************************/

/* reads an int from a string */
static int read_int(const char * str)
{
	char n[32];
	int i = 0;
	memset(n, 0, sizeof(n));

	while (isdigit(str[i])) {
		n[i] = str[i];
		i++;
	}

	return atoi(n);
}

/* reads the indices for a line */
static void read_vertices(int a[3], int b[3], int c[3], const char * str)
{
	int * d[3] = {a, b, c};
	int vidx = 0;
	int cidx = 0;
	int i = 0;
	int j = 0;

	for (; j < 3; j++) {
		a[j] = - 1;
		b[j] = - 1;
		c[j] = - 1;
	}

	/* fast forward to first digit */
	while(!isdigit(str[i]))
		i++;

	while (str[i] != '\n' && str[i] != EOF && str[i] != '\0') {
		if (isdigit(str[i])) {
			d[vidx][cidx] = read_int(str + i) - 1;
			while (isdigit(str[i]))
				i++;
			continue;
		} else if (str[i] == '/') {
			cidx++;
		} else if (str[i] == ' ') {
			cidx = 0;
			vidx++;	
		}

		i++;
	}
}

/* reads in a face given a line from an .obj file */
static void read_face(struct obj_scene * scene, const char * line)
{
	int a[3], b[3], c[3];
	int f[4];
	int m;
	struct obj_material * mat;

	f[0] = (int)scene->vertices->count + 0;
	f[1] = (int)scene->vertices->count + 1;
	f[2] = (int)scene->vertices->count + 2;

	if (!strlen(current_material)) {
		f[3] = -1;
	} else {
		m = find_material_index(scene, current_material);
	
		if (m < 0) {
			mat = create_material(current_material);
			array_push(scene->materials, &mat);
		}

		f[3] = find_material_index(scene, current_material);
	}

	array_push(scene->faces, f);
	read_vertices(a, b, c, line);
	array_push(scene->vertices, a);
	array_push(scene->vertices, b);
	array_push(scene->vertices, c);
}

static void read_position(struct obj_scene * scene, const char * line)
{
	float p[3];
	assert(3 == sscanf(line, "v %f %f %f", p, p + 1, p + 2));
	array_push(scene->positions, p);
}

static void read_normal(struct obj_scene * scene, const char * line)
{
	float p[3];
	assert(3 == sscanf(line, "vn %f %f %f", p, p + 1, p + 2));
	array_push(scene->normals, p);
}

static void read_tex_coord(struct obj_scene * scene, const char * line)
{
	float p[2];
	assert(2 == sscanf(line, "vt %f %f", p, p + 1));
	array_push(scene->tex_coords, p);
}

static void read_material(const char * line)
{
	sscanf(line, "usemtl %s", current_material);
}

static void read_mtl_file(const char * line)
{
	sscanf(line, "mtllib %s", mtl_file_name);
}

static void process_line(struct obj_scene * scene, const char * line)
{
	char l[MAX_STRLEN];
	trim_line(l, line);

	if (strstr(l, "f ") == l) {
		read_face(scene, l);	
	} else if (strstr(l, "v ") == l) {
		read_position(scene, l);
	} else if (strstr(l, "vn ") == l) {
		read_normal(scene, l);	
	} else if (strstr(l, "vt ") == l) {
		read_tex_coord(scene, l);		
	} else if (strstr(l, "usemtl") == l) {
		read_material(l);
	} else if (strstr(l, "mtllib") == l) {
		read_mtl_file(l);
	}
}

static void process_mtl_file(struct obj_scene * scene, const char * line)
{
	char l[MAX_STRLEN];
	char mn[MAX_STRLEN];
	int m = - 1;
	trim_line(l, line);

	if (!strlen(l))
		return; /* ignore empty lines */

	if (strstr(l, "newmtl") == l) {
		assert(1 == sscanf(l, "newmtl %s", mn));
		sscanf(l, "newmtl %s", current_material);
		return;
	}

	m = find_material_index(scene, current_material);

	if (m < 0)
		return;

	create_material_entry(scene, m, l);
}

void dump(obj_scene_t scene)
{
	int i;
	int * f;
	int * v0;
	int * v1;
	int * v2;
	float * v;
	
	for (i = 0; i < scene->faces->count; i++) {
		f = array_get(scene->faces, i);
		v0 = array_get(scene->vertices, f[0]);
		v1 = array_get(scene->vertices, f[1]);
		v2 = array_get(scene->vertices, f[2]);
		printf("f %d/%d/%d %d/%d/%d %d/%d/%d\n",
				v0[0], v0[1], v0[2],
				v1[0], v1[1], v1[2],
				v2[0], v2[1], v2[2]
		       );
	}

	for (i = 0; i < scene->positions->count; i++) {
		v = array_get(scene->positions, i);
		
		printf("v %f %f %f\n", v[0], v[1], v[2]);
		
	}
	
}

/*****************************************************************************/

obj_scene_t obj_scene_create_with_file(const char * file_name, const char * path)
{
	struct obj_scene * s = calloc(1, sizeof(*s));
	char pfn[MAX_STRLEN] = {'\0'}; /* path + filename */
	FILE * f = NULL;
	char l[MAX_STRLEN];
	assert(s);

	/* reset globals */
	current_material[0] = '\0';
	mtl_file_name[0] = '\0';

	if (path) {
		strcpy(pfn, path);
		strcat(pfn, "/");
	}

	strcat(pfn, file_name);
	f = fopen(pfn, "r");
	assert(f);

	/* create arrays for the scene */
	s->faces = array_create(sizeof(int[4]), 0);
	s->vertices = array_create(sizeof(int[3]), 0);
	s->positions = array_create(sizeof(float[3]), 0);
	s->tex_coords = array_create(sizeof(float[2]), 0);
	s->normals = array_create(sizeof(float[3]), 0);
	s->materials = array_create(sizeof(struct obj_material *), 0);

	/* read in file line by line */
	while (fgets(l, MAX_STRLEN, f)) 
		process_line(s, l);

	fclose(f);

	//dump(s);
	
	if (!strlen(mtl_file_name)) 
		return s; /* if there is no mtl file, we are done */

	/* open the mtl file */
	pfn[0] = '\0';

	if (path) {
		strcpy(pfn, path);
		strcat(pfn, "/");
	}

	strcat(pfn, mtl_file_name);
	
	f = fopen(pfn, "r");
	assert(f);

	/* process mtl file */
	while (fgets(l, MAX_STRLEN, f))
		process_mtl_file(s, l);

	fclose(f);
	return s;
}

void obj_scene_release(obj_scene_t scene)
{
	int i = 0;
	struct obj_material ** mat = NULL;
	assert(scene);
	array_release(scene->faces);
	array_release(scene->vertices);
	array_release(scene->positions);
	array_release(scene->tex_coords);
	array_release(scene->normals);

	for (; i < (int)scene->materials->count; i++) {
		mat = array_get(scene->materials, i);
		release_material(*mat);
	}

	array_release(scene->materials);
	free(scene);
}

int obj_scene_get_face_count(obj_scene_t scene)
{
	return (int)scene->faces->count;
}

int obj_scene_get_vertex(obj_scene_t scene, int * vertex, int face, int i)
{
	int * f;

	if (face < 0 || face >= (int)scene->faces->count || i < 0 || i > 2)
		return 0;

	f = array_get(scene->faces, face);
	*vertex = f[i];
	return 1;
}

int obj_scene_get_position(obj_scene_t scene, float pos[3], int vertex)
{
	int * vert;

	if (vertex < 0 || vertex >= scene->vertices->count)
		return 0;

	vert = array_get(scene->vertices, vertex);
	memcpy(pos, array_get(scene->positions, vert[0]), sizeof(float[3]));
	return 1;
}

int obj_scene_get_normal(obj_scene_t scene, float normal[3], int vertex)
{
	int * vert;

	if (vertex < 0 || vertex >= scene->vertices->count)
		return 0;

	vert = array_get(scene->vertices, vertex);

	if (vert[2] < 0)
		return 0;

	memcpy(normal, array_get(scene->normals, vert[2]), sizeof(float[3]));
	return 1;
}

int obj_scene_get_tex_coord(obj_scene_t scene, float tc[2], int vertex)
{
	int * vert;

	if (vertex < 0 || vertex >= scene->vertices->count)
		return 0;

	vert = array_get(scene->vertices, vertex);

	if (vert[2] < 0)
		return 0;

	memcpy(tc, array_get(scene->positions, vert[2]), sizeof(float[2]));
	return 1;
}

int obj_scene_get_material(obj_scene_t scene, int * material, 
	int face)
{
	int * f;

	if (face < 0 || face >= (int)scene->faces->count)
		return 0;

	f = array_get(scene->faces, face);
	*material = f[3];
	return 1;
}

int obj_scene_material_get_element_for_entry_as_float(obj_scene_t scene, 
	int material, float * element, const char * entry, int i)
{
	struct obj_material ** m = NULL;
	struct obj_material_entry * matent;

	if (material < 0 || material >= scene->materials->count)
		return 0;

	m = array_get(scene->materials, material);
	matent = find_material_entry(*m, entry);
	
	if (!matent)
		return 0;

	if (i < 0 || i >= matent->element_count)
		return 0;

	if (element)
		*element = atof(matent->elements[i]);

	return 1;
}

int obj_scene_material_get_element_for_entry_as_int(obj_scene_t scene, 
	int material, int * element, const char * entry, int i)
{
	struct obj_material ** m = NULL;
	struct obj_material_entry * matent;

	if (material < 0 || material >= scene->materials->count)
		return 0;

	m = array_get(scene->materials, material);
	matent = find_material_entry(*m, entry);
	
	if (!matent)
		return 0;

	if (i < 0 || i >= matent->element_count)
		return 0;

	if (element)
		*element = atoi(matent->elements[i]);

	return 1;
}

int obj_scene_material_get_element_for_entry_as_scene(obj_scene_t scene,
	int material, char ** element, const char * entry, int i)
{
	struct obj_material ** m = NULL;
	struct obj_material_entry * matent;

	if (material < 0 || material >= scene->materials->count)
		return 0;

	m = array_get(scene->materials, material);
	matent = find_material_entry(*m, entry);
	
	if (!matent)
		return 0;

	if (i < 0 || i >= matent->element_count)
		return 0;

	if (element)
		*element = matent->elements[i];
	return 1;
}

int obj_scene_get_material_count(obj_scene_t scene)
{
	return (int)scene->materials->count;
}
