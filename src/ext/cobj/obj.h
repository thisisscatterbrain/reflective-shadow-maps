/*
 * obj.h
 */
#ifndef OBJ_H
#define OBJ_H

#ifdef __cplusplus
extern "C"
{
#endif

typedef struct obj_scene * obj_scene_t;

/* 
 * Creates a obj scene given a file, which is specified by [file_name] 
 * and [path]. If [path] is  zero, the file is assumed to be located
 * in the current directory,
 */
obj_scene_t obj_scene_create_with_file(const char * file_name,
	const char * path);

/* 
 * Releases a obj scene
 */
void obj_scene_release(obj_scene_t scene);

/* 
 * Gets the number of faces stored in the scene.
 */
int obj_scene_get_face_count(obj_scene_t scene);

/* 
 * Gets the i-th vertex for a face. i must eiter 0,1 or 2 (only triangles are stored).
 * The vertex is stored in [vertex], returns 0 if the vertex does not exist.
 */
int obj_scene_get_vertex(obj_scene_t scene, int * vertex, int face, int i);

/* 
 * Gets the position for a vertex and stores it in [vertex]. Returns 0 if
 * vertex does not exist.
 */
int obj_scene_get_position(obj_scene_t scene, float pos[3], int vertex);

/* 
 * Gets the normal for a vertex and stores it in [normal]. Returns 0 if
 * vertex or the normal for the vertex does not exist.
 */
int obj_scene_get_normal(obj_scene_t scene, float normal[3], int vertex);

/* 
 * Gets the texture coordinate for a vertex and stores it in [tx]. Returns 0 if
 * vertex or the texture coordinate for the vertex does not exist 
 */
int obj_scene_get_tex_coord(obj_scene_t scene, float tc[3], int vertex);

/* 
 * Gets the material for a face and stores it in [material]. Returns 0 if 
 * the face or the material for the face does not exist. 
 */
int obj_scene_get_material(obj_scene_t scene, int * material, 
	int face);

int obj_scene_get_material_count(obj_scene_t scene);
int obj_scene_material_get_element_for_entry_as_float(obj_scene_t scene, 
	int material, float * element, const char * entry, int i);


int obj_scene_material_get_element_for_entry_as_int(obj_scene_t scene, 
	int material, int * element, const char * entry, int i);

int obj_scene_material_get_element_for_entry_as_string(obj_scene_t scene, 
	int material, char ** element, const char * entry, int i);

#ifdef __cplusplus
}
#endif


#endif /* end of include guard: OBJ_H */
