#ifndef CONFIG_H
#define CONFIG_H

#ifdef CONFIG
	#define CONFIG_EXTERN
#else
	#define CONFIG_EXTERN extern const
#endif

struct {
	struct {
		char filename[256];
		char path[256];
	} scene;

} config;

void config_initialize(int argc, char * argv[]);
void config_finalize();

#endif /* end of include guard : config.h */