#include <stdio.h>
#include "config.h"
#include "glfw-app.h"
#include "scene.h"

static void initialize()
{
	scene_initialize();
}

int main(int argc, char * argv[])
{
	config_initialize(argc, argv);
	glfw_app_initialize();
	initialize();
	glfw_app_run();
	glfw_app_finalize();
	return 0;
}