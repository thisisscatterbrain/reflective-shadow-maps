#ifndef SCENE_H
#define SCENE_H

#include <GL/glew.h>
#include <string>
#include <vector>

// Definition of a scene.
class Scene {
public:
	class Material {
		float diffuse_reflectance[3];
	};

	class SubMesh {
	public:
		GLuint vao;
		GLuint vbo;
		Material material;
	};

	// Loads the scene from an .obj file.
	Scene(const std::string& filename);
	~Scene();

	std::vector<SubMesh*> meshes;

private:
	Scene();
	Scene(const Scene&);
	Scene& operator=(const Scene&);
};

#endif /* end of include guard : scene.h */