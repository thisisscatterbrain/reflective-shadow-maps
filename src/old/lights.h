// lights.h
#ifndef LIGHTS_H
#define LIGHTS_H

// A point light that emits radiance equally in all directions.
struct PointLight {
	PointLight();
	PointLight(float position[3], float radiance);
	~PointLight();

	float position[3];
	float radiance;
};

#endif /* end of include guard : lights.h */