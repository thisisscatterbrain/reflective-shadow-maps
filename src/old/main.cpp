#include <cstdio>
#include "application.h"

int main(int argc, char * argv)
{
	glfw::Application * app = glfw::Application::get_instance();
	app->initialize();
	app->run();
	app->finalize();
	return 0;
}