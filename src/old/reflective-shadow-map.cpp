#include "reflective-shadow-map.h"

ReflectiveShadowMap::ReflectiveShadowMap() : ReflectiveShadowMap(512, 512) {}

ReflectiveShadowMap::ReflectiveShadowMap(int width, int height) : 
	width(width), height(height), handle(0), type(TYPE_NONE) {}

ReflectiveShadowMap::~ReflectiveShadowMap() 
{
	if (type)
		glDeleteTextures(1, &this->handle);
}