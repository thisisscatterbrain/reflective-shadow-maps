// application.h
//
// GLFW convenience wrapper.
#ifndef APPLICATION_H
#define APPLICATION_H

#include <vector>
#include <string>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

namespace glfw {

// Systems handle input events and can update themselves every frame
class System {
public:
	virtual void update(float dt) {};
};

class Application {
public:
	static Application * get_instance();
	
	// initializes the application. Creates a window and OpenGL context.
	void initialize();
	void finalize();

	void set_title(const std::string& title);
	void set_window_resolution(int width, int height);

	// Pushes a system to the application. Systems update themselves in
	// the order they were pushed to the application.
	void push(System * system);
	void run();

private:
	Application();
	Application(const Application&);
	Application& operator=(const Application&);
	~Application();


	std::vector<System*> systems;
	int width, height;
	std::string title;
	GLFWwindow * window;
	int is_initialized;
};

}

#endif /* end of include guard : application*/