#include "application.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <cassert>

glfw::Application::Application() : is_initialized(0), 
	title("GLFW Application"), width(1280), height(720) {}
glfw::Application::~Application() {}

glfw::Application * glfw::Application::get_instance()
{
	static Application * instance = NULL;

	if (!instance)
		instance = new Application();

	return instance;
}

void glfw::Application::initialize()
{
	if (this->is_initialized)
		return;

	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	this->window = glfwCreateWindow(this->width, this->height, 
		this->title.c_str(), NULL, NULL);
	glfwMakeContextCurrent(this->window);

	glewExperimental = GL_TRUE;
	glewInit();
	glGetError();
	assert(this->window);
}

void glfw::Application::finalize()
{
	glfwTerminate();
}

void glfw::Application::run()
{
	while (!glfwWindowShouldClose(this->window)) {
		glfwSwapBuffers(this->window);
		glfwPollEvents();
	}
}

void glfw::Application::set_title(const std::string& title)
{
	this->title = title;
}

void glfw::Application::set_window_resolution(int width, int height)
{
	this->width = width;
	this->height = height;
}