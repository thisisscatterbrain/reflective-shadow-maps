#include "lights.h"
#include <memory>

PointLight::PointLight() : radiance(0.0f)
{
	memset(this->position, 0, sizeof(this->position));
}

PointLight::PointLight(float position[3], float radiance) : radiance(radiance)
{
	memcpy(this->position, position, sizeof(this->position));
}

PointLight::~PointLight() {}