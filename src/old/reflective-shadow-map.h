// reflective-shadow-map.h
#ifndef REFLECTIVE_SHADOW_MAP_H
#define REFLECTIVE_SHADOW_MAP_H

#include <GL/glew.h>
#include "lights.h"

class ReflectiveShadowMap {
public:
	//---------------------------------------------------------------------	

	// This enum represents the type of the RSM, depending on the lights
	// the RSM is generated for different textures are used.
	enum {
		TYPE_NONE = 0,
		TYPE_CUBE_MAP,
		TYPE_TEX_2D
	};

	//---------------------------------------------------------------------
	// Enables the use of RSMs. Call this once before using RSMs.
	static void enable();

	// Disables the use of RSMs. Call this when cleaning up.
	static void disable();

	//---------------------------------------------------------------------
	ReflectiveShadowMap();
	ReflectiveShadowMap(int width, int height);
	~ReflectiveShadowMap();
	void generate(const PointLight& light);

	//---------------------------------------------------------------------
	GLuint handle;		// texture handle for the rsm
	int type;		// type of the rsm (see above)
	int width, height;

private:
	ReflectiveShadowMap(const ReflectiveShadowMap&);
	ReflectiveShadowMap& operator=(const ReflectiveShadowMap&);
};

#endif /* end of include guard : reflective-shadow-map.h */